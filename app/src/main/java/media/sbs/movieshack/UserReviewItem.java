package media.sbs.movieshack;

import java.io.Serializable;

/**
 * Created by ss on 4/11/16.
 */
public class UserReviewItem implements Serializable {
    private int mMovieId;
    private String author;
    private String content;
    private String url;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getMovieId() {
        return mMovieId;
    }

    public void setMovieId(int movieId) {
        mMovieId = movieId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
