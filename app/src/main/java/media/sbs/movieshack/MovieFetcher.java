package media.sbs.movieshack;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ss on 3/30/16.
 */

public class MovieFetcher {

    private final String TAG = "MovieFetcher";

    private final String MOVIE_DB_API_KEY = BuildConfig.THE_MOVIE_DB_API_KEY;
    private final String MOVIE_API_URL = "https://api.themoviedb.org/3/movie/";
    private final String API_KEY_PARAM = "api_key";

    public String getUrlString(String urlString) throws IOException {
        BufferedReader reader = null;
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        try {
            InputStream inputStream = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseCode() + ": with: " + urlString);
            }
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // add newline to make reading JSON easier
                buffer.append(line + "\n");
            }
            if (buffer.length() == 0) {
                return null;
            }
            return buffer.toString();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException ioe) {
                    Log.e(TAG, "Error closing stream", ioe);
                }
            }

        }
    }

    public List<MovieItem> fetchJSON(String menuOption) {
        List<MovieItem> movieItems = new ArrayList<>();
        String jsonString = null;
        try {
            String extraApiPath = menuOption;

            String url = Uri.parse(MOVIE_API_URL)
                    .buildUpon()
                    .appendPath(extraApiPath)
                    .appendQueryParameter(API_KEY_PARAM, MOVIE_DB_API_KEY)
                    .build().toString();
            jsonString = getUrlString(url);
            Log.i(TAG, "Received JSON: " + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseMovies(movieItems, jsonBody);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch URL", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }
        return movieItems;
    }

    private void parseMovies(List<MovieItem> movieItems, JSONObject jsonBody) throws IOException, JSONException {
        final String IMAGE_URL = "http://image.tmdb.org/t/p/w185";
        JSONArray moviesJsonArray = jsonBody.getJSONArray("results");

        for (int i = 0; i < moviesJsonArray.length(); i++) {
            JSONObject movieJsonObject = moviesJsonArray.getJSONObject(i);
            String posterPath = movieJsonObject.getString("poster_path");
            MovieItem item = new MovieItem();
            item.setMovieId(movieJsonObject.getInt("id"));
            item.setPosterPath(IMAGE_URL + posterPath);
            item.setOverview(movieJsonObject.getString("overview"));
            item.setTitle(movieJsonObject.getString("original_title"));
            item.setReleaseDate(movieJsonObject.getString("release_date"));
            item.setRating(movieJsonObject.getDouble("vote_average"));
            movieItems.add(item);
        }
    }

    private void parseTrailers(List<TrailerItem> trailerItems, JSONObject jsonBody) throws IOException, JSONException {
        JSONArray trailersJsonArray = jsonBody.getJSONArray("results");
        final int movieId = jsonBody.getInt("id");

        for (int i = 0; i < trailersJsonArray.length(); i++) {
            JSONObject trailerJsonObject = trailersJsonArray.getJSONObject(i);
            TrailerItem item = new TrailerItem();
            item.setKey(trailerJsonObject.getString("key"));
            item.setMovieId(movieId);
            item.setName(trailerJsonObject.getString("name"));
            item.setSite(trailerJsonObject.getString("site"));
            trailerItems.add(item);
        }
    }

    private void parseUserReviews(List<UserReviewItem> userReviewItems, JSONObject jsonBody) throws IOException, JSONException {
        JSONArray userReviewsJsonArray = jsonBody.getJSONArray("results");
        final int movieId = jsonBody.getInt("id");

        for (int i = 0; i < userReviewsJsonArray.length(); i++) {
            JSONObject userReviewJsonObject = userReviewsJsonArray.getJSONObject(i);
            UserReviewItem item = new UserReviewItem();
            item.setMovieId(movieId);
            item.setAuthor(userReviewJsonObject.getString("author"));
            item.setContent(userReviewJsonObject.getString("content"));
            item.setUrl(userReviewJsonObject.getString("url"));
            userReviewItems.add(item);
        }
    }

    public List<TrailerItem> fetchTrailers(String movieId) {
        List<TrailerItem> trailerItems = new ArrayList<>();
        String jsonString = null;
        String trailersPath = "videos";

        try {
            String url = Uri.parse(MOVIE_API_URL)
                    .buildUpon()
                    .appendPath(movieId)
                    .appendPath(trailersPath)
                    .appendQueryParameter(API_KEY_PARAM, MOVIE_DB_API_KEY)
                    .build().toString();
            jsonString = getUrlString(url);
            Log.i(TAG, "Received JSON: " + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseTrailers(trailerItems, jsonBody);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch URL", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }
        return trailerItems;
    }

    public List<UserReviewItem> fetchUserReviews(String movieId) {
        List<UserReviewItem> userReviewItems = new ArrayList<>();
        String jsonString = null;
        String userReviewsPath = "reviews";

        try {
            String url = Uri.parse(MOVIE_API_URL)
                    .buildUpon()
                    .appendPath(movieId)
                    .appendPath(userReviewsPath)
                    .appendQueryParameter(API_KEY_PARAM, MOVIE_DB_API_KEY)
                    .build().toString();
            jsonString = getUrlString(url);
            Log.i(TAG, "fetchUserReviews - received JSON: " + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseUserReviews(userReviewItems, jsonBody);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch URL", ioe);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }
        return userReviewItems;
    }
}
