package media.sbs.movieshack;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ss on 3/30/16.
 */

public class MovieDetailFragment extends Fragment {
    private static final String TAG = MovieDetailFragment.class.getSimpleName();
    private static final String ARG_MOVIE_ITEM = "movieItem";
    private MovieItem mMovieItem;
    private View mMovieDetailView;
    private Callbacks mCallbacks;
    private Button mFavoriteButton;

    public interface Callbacks {
        void onAddFavorite(MovieItem movieItem);
    }

    public MovieDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ( getArguments() != null) {
        mMovieItem = (MovieItem) getArguments().getSerializable(ARG_MOVIE_ITEM);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        try {
            activity = (Activity)context;
            mCallbacks = (Callbacks)activity;
        } catch (ClassCastException cce) {
            throw new ClassCastException(TAG + " must implement Callbacks interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mMovieDetailView = inflater.inflate(R.layout.fragment_movie_detail, container, false);

        Intent intent = getActivity().getIntent();
        if (intent != null && intent.hasExtra(ARG_MOVIE_ITEM)) {
            mMovieItem = (MovieItem) intent.getSerializableExtra(ARG_MOVIE_ITEM);
        }

        if (mMovieItem != null) {
            updateContent();
        }

        return mMovieDetailView;
    }

    public void updateContent() {

        TextView overview = (TextView) mMovieDetailView.findViewById(R.id.movie_detail_overview);
        ImageView poster = (ImageView) mMovieDetailView.findViewById(R.id.movie_detail_poster);
        TextView title = (TextView) mMovieDetailView.findViewById(R.id.movie_detail_title);
        TextView rating = (TextView) mMovieDetailView.findViewById(R.id.movie_detail_user_rating);
        TextView releaseDate = (TextView) mMovieDetailView.findViewById(R.id.movie_detail_release_date);
        mFavoriteButton = (Button) mMovieDetailView.findViewById(R.id.movie_detail_mark_fav_button);

        overview.setText(mMovieItem.getOverview());
        Picasso.with(getActivity())
                .load(mMovieItem.getPosterPath())
                .placeholder(R.drawable.movieshack_mockup)
                .into(poster);
        title.setText(mMovieItem.getTitle());
        rating.setText(String.valueOf(mMovieItem.getRating()));
        releaseDate.setText(mMovieItem.getReleaseDate());

        // check to see if movie is already favorite
        if (!isFavorite()) {
            mFavoriteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addFavorite();
                    removeFavoriteButton();
                }
            });
        } else {
            removeFavoriteButton();
        }

        new FetchExtraContent().execute(Integer.toString(mMovieItem.getMovieId()));
    }

    private void removeFavoriteButton() {
        mFavoriteButton.setVisibility(View.GONE);
    }

    private class FetchExtraContent extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            String movieId = params[0];
            MovieFetcher movieFetcher = new MovieFetcher();
            List<TrailerItem> trailerItems = movieFetcher.fetchTrailers(movieId);
            mMovieItem.setTrailers(trailerItems);
            List<UserReviewItem> userReviewItems = movieFetcher.fetchUserReviews(movieId);
            mMovieItem.setUserReviews(userReviewItems);
            return null;
        }

        @Override
        protected void onPostExecute(Void result){
            setupExtraContent();
        }
    }

    private void setupExtraContent() {
        setupTrailerContent();
        setupUserReviewContent();
    }

    private void setupTrailerContent() {
        List<TrailerItem> trailers = mMovieItem.getTrailers();
        LinearLayout trailersView = (LinearLayout) mMovieDetailView.findViewById(R.id.movie_detail_trailers);
        for(int i = 0; i < trailers.size(); i++) {
            final TrailerItem trailerItem = trailers.get(i);
            View trailerView = View.inflate(getContext(), R.layout.movie_detail_trailers_view, null);
            ImageButton trailerButton = (ImageButton) trailerView.findViewById(R.id.movie_detail_trailer_button);
            trailerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + trailerItem.getKey()));
                        startActivity(intent);
                    } catch (ActivityNotFoundException anfe) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + trailerItem.getKey()));
                        startActivity(intent);
                    }

                }
            });
            TextView textView = (TextView) trailerView.findViewById(R.id.movie_detail_trailer_container_text_view);
            textView.setText("trailer " + (i + 1));
            trailersView.addView(trailerView);
        }
    }

    private void setupUserReviewContent() {
        List<UserReviewItem> userReviews = mMovieItem.getUserReviews();
        LinearLayout userReviewsView = (LinearLayout) mMovieDetailView.findViewById(R.id.movie_detail_user_reviews);
        for(int i = 0; i < userReviews.size(); i++) {
            UserReviewItem userReview = userReviews.get(i);
            View userReviewView = View.inflate(getContext(), R.layout.movie_detail_user_reviews_view, null);
            TextView authorTextView = (TextView) userReviewView.findViewById(R.id.movie_detail_user_review_author);
            TextView contentTextView = (TextView) userReviewView.findViewById(R.id.movie_detail_user_review_content);
            authorTextView.setText(userReview.getAuthor());
            contentTextView.setText(userReview.getContent());
            userReviewsView.addView(userReviewView);
        }
    }

    private void addFavorite() {
        mCallbacks.onAddFavorite(mMovieItem);
    }

    private Boolean isFavorite() {
        Boolean favorite = mMovieItem.isFavorite();
        if (favorite == null) {
            if (Favorites.get(getContext()).getFavorite(mMovieItem.getMovieId()) == null) {
                return false;
            } else {
                mMovieItem.setIsFavorite(true);
                return true;
            }
        } else {
            return favorite;
        }
    }
}
