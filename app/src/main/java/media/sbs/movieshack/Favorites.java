package media.sbs.movieshack;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import media.sbs.movieshack.database.FavoriteCursorWrapper;
import media.sbs.movieshack.database.FavoriteDbHelper;
import media.sbs.movieshack.database.FavoriteDbSchema.FavoriteTable;

/**
 * Created by ss on 4/12/16.
 */
public class Favorites {
    private static Favorites sFavorites;

    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static Favorites get(Context context) {
        if (sFavorites == null) {
            sFavorites = new Favorites(context);
        }
        return sFavorites;
    }

    private Favorites(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new FavoriteDbHelper(mContext)
                .getWritableDatabase();
    }

    public void addFavorite(MovieItem movieItem) {
        ContentValues values = getContentValues(movieItem);

        mDatabase.insert(FavoriteTable.NAME, null, values);
    }

    public List<MovieItem> getFavorites() {
        List<MovieItem> movieItems = new ArrayList<>();

        FavoriteCursorWrapper cursor = queryFavorites(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                movieItems.add(cursor.getFavorite());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return movieItems;
    }

    public MovieItem getFavorite(int movieId) {
        FavoriteCursorWrapper cursor = queryFavorites(
                FavoriteTable.Cols.MOVIE_ID + " = ?",
                new String[] { Integer.toString(movieId)}
        );

        try {
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            return cursor.getFavorite();
        } finally {
            cursor.close();
        }
    }

    public void updateFavorite(MovieItem movieItem) {
        String movieIdString = Integer.toString(movieItem.getMovieId());
        ContentValues values = getContentValues(movieItem);

        mDatabase.update(FavoriteTable.NAME, values,
                FavoriteTable.Cols.MOVIE_ID + " = ?",
                new String[] { movieIdString });
    }

    private static ContentValues getContentValues(MovieItem movieItem) {
        ContentValues values = new ContentValues();
        values.put(FavoriteTable.Cols.MOVIE_ID, Integer.toString(movieItem.getMovieId()));
        values.put(FavoriteTable.Cols.TITLE, movieItem.getTitle());
        values.put(FavoriteTable.Cols.RELEASE_DATE, movieItem.getReleaseDate());
        values.put(FavoriteTable.Cols.OVERVIEW, movieItem.getOverview());
        values.put(FavoriteTable.Cols.POSTER_PATH, movieItem.getPosterPath());
        values.put(FavoriteTable.Cols.RATING, movieItem.getRating());

        return values;
    }

    private FavoriteCursorWrapper queryFavorites(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                FavoriteTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        return new FavoriteCursorWrapper(cursor);
    }
}
