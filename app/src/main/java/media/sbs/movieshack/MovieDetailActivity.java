package media.sbs.movieshack;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by ss on 3/31/16.
 */
public class MovieDetailActivity extends AppCompatActivity implements MovieDetailFragment.Callbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_shack);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new MovieDetailFragment())
                    .commit();

        }
    }

    @Override
    public void onAddFavorite(MovieItem movieItem) {
        Favorites.get(this).addFavorite(movieItem);
    }
}
