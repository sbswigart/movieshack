package media.sbs.movieshack;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import java.io.Serializable;

public class MovieShackActivity extends AppCompatActivity implements MovieShackFragment.Callbacks, MovieDetailFragment.Callbacks {
    private static final String ARG_MOVIE_ITEM = "movieItem";

    @LayoutRes
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new MovieShackFragment())
                    .commit();
        }
    }

    @Override
    public void onMovieSelected(MovieItem movieItem){
        if (findViewById(R.id.movie_detail_fragment_container) == null) {
            Intent intent = new Intent(MovieShackActivity.this, MovieDetailActivity.class).putExtra("movieItem", (Serializable)movieItem);
            startActivity(intent);
        } else {

            MovieDetailFragment movieDetailFragment = new MovieDetailFragment();
            Bundle args = new Bundle();
            args.putSerializable(ARG_MOVIE_ITEM, movieItem);
            movieDetailFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.movie_detail_fragment_container, movieDetailFragment)
                .commit();

        }
    }

    @Override
    public void onAddFavorite(MovieItem movieItem) {
        Favorites.get(this).addFavorite(movieItem);
        MovieShackFragment movieShackFragment = (MovieShackFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        movieShackFragment.updateFavorites();
    }
}
