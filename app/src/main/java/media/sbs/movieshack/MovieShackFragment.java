package media.sbs.movieshack;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ss on 3/29/16.
 */
public class MovieShackFragment extends Fragment {
    private final String TAG = MovieShackFragment.class.getSimpleName();
    private RecyclerView mMoviePosterRecyclerView;
    private MoviePosterAdapter mMoviePosterAdapter;
    private List<MovieItem> mItems = new ArrayList<>();
    private Callbacks mCallbacks;

    public interface Callbacks {
        void onMovieSelected(MovieItem movieItem);
    }

    public MovieShackFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        String defaultSorting = "popular";
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        // remove
        Favorites.get(getActivity());

        new FetchJson().execute(defaultSorting);
    }

    // remove... example of updating database. maybe put boolean datatype in movie item to mark it as favorite
//    @Override
//    public void onPause() {
//        super.onPause();
//
//        Favorites.get(getActivity()).updateFavorite();
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_movie_shack, container, false);
        mMoviePosterRecyclerView = (RecyclerView) v.findViewById(R.id.fragment_movie_shack_recycler_view);
        mMoviePosterRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        setupAdapter();

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        try {
            activity = (Activity)context;
            mCallbacks = (Callbacks)activity;
        } catch (ClassCastException cce) {
            throw new ClassCastException(TAG + " must implement Callbacks interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragement_movie_poster_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String popularOption = "popular";
        String topRatedOption = "top_rated";
        switch (item.getItemId()) {
            case R.id.menu_popular_item:
                new FetchJson().execute(popularOption);
                return true;
            case R.id.menu_top_rated_item:
                new FetchJson().execute(topRatedOption);
                return true;
            case R.id.menu_favorite_item:
                updateFavorites();
                return true;
            default:
                super.onOptionsItemSelected(item);
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupAdapter() {
        if (isAdded()) {
            mMoviePosterRecyclerView.setAdapter(new MoviePosterAdapter(mItems));
        }
    }

    private void addFavorite(MovieItem movieItem) {
        Favorites.get(getActivity()).addFavorite(movieItem);
    }

    public void updateFavorites() {
        Favorites favorites = Favorites.get(getActivity());
        List<MovieItem> favoriteMovies = favorites.getFavorites();

        if (mMoviePosterAdapter == null) {
            mMoviePosterAdapter = new MoviePosterAdapter(favoriteMovies);
            mMoviePosterRecyclerView.setAdapter(mMoviePosterAdapter);
        } else {
            mMoviePosterAdapter.setMovieItems(favoriteMovies);
            mMoviePosterAdapter.notifyDataSetChanged();
        }
    }

    private class MoviePosterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView mMoviePosterView;
        private MovieItem mMovieItem;

        public MoviePosterHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mMoviePosterView = (ImageView) itemView.findViewById(R.id.fragment_movie_poster_image_view);
        }

        public void bindMovieItem(MovieItem movieItem) {
            mMovieItem = movieItem;
            Picasso.with(getActivity())
                    .load(mMovieItem.getPosterPath())
                    .placeholder(R.drawable.movieshack_mockup)
                    .error(R.drawable.movieshack_mockup)
                    .into(mMoviePosterView);
        }

        @Override
        public void onClick(View v) {
            mCallbacks.onMovieSelected(mMovieItem);
        }
    }

    private class MoviePosterAdapter extends RecyclerView.Adapter<MoviePosterHolder> {

        private List<MovieItem> mMovieItems;

        public MoviePosterAdapter(List<MovieItem> movieItems) {
            mMovieItems = movieItems;
        }

        @Override
        public MoviePosterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(R.layout.list_item_movie_poster, parent, false);
            return new MoviePosterHolder(view);
        }

        @Override
        public void onBindViewHolder(MoviePosterHolder holder, int position) {
            MovieItem movieItem = mMovieItems.get(position);
            holder.bindMovieItem(movieItem);
        }

        @Override
        public int getItemCount() {
            return mMovieItems.size();
        }

        public void setMovieItems(List<MovieItem> movieItems) {
            mMovieItems = movieItems;
        }
    }

    private class FetchJson extends AsyncTask<String, Void, List<MovieItem>> {

        private String defaultOption = "popular";
        @Override
        protected List<MovieItem> doInBackground(String... params) {
           return new MovieFetcher().fetchJSON(params[0]);
        }

        @Override
        protected void onPostExecute(List<MovieItem> movieItems){
            mItems = movieItems;
            setupAdapter();
        }
    }

}
