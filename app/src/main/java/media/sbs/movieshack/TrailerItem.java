package media.sbs.movieshack;

import java.io.Serializable;

/**
 * Created by ss on 4/10/16.
 */
public class TrailerItem implements Serializable {

    private int mMovieId;
    private String mSite;
    private String mName;
    private String mKey;

    public String getKey() {
        return mKey;
    }

    public void setKey(String key) {
        mKey = key;
    }

    public int getMovieId() {
        return mMovieId;
    }

    public void setMovieId(int movieId) {
        mMovieId = movieId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSite() {
        return mSite;
    }

    public void setSite(String site) {
        mSite = site;
    }
}
