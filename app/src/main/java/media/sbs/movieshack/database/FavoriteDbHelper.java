package media.sbs.movieshack.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import media.sbs.movieshack.database.FavoriteDbSchema.FavoriteTable;

/**
 * Created by ss on 4/12/16.
 */
public class FavoriteDbHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "crimeBase.db";

    public FavoriteDbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + FavoriteTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                FavoriteTable.Cols.MOVIE_ID + ", " +
                FavoriteTable.Cols.OVERVIEW + ", " +
                FavoriteTable.Cols.POSTER_PATH + ", " +
                FavoriteTable.Cols.RELEASE_DATE + ", " +
                FavoriteTable.Cols.TITLE + ", " +
                FavoriteTable.Cols.RATING +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
