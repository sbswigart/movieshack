package media.sbs.movieshack.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import media.sbs.movieshack.MovieItem;
import media.sbs.movieshack.database.FavoriteDbSchema.FavoriteTable;

/**
 * Created by ss on 4/12/16.
 */
public class FavoriteCursorWrapper extends CursorWrapper {

    public FavoriteCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public MovieItem getFavorite() {
        String movieId = getString(getColumnIndex(FavoriteTable.Cols.MOVIE_ID));
        String title = getString(getColumnIndex(FavoriteTable.Cols.TITLE));
        String releaseDate = getString(getColumnIndex(FavoriteTable.Cols.RELEASE_DATE));
        String posterPath = getString(getColumnIndex(FavoriteTable.Cols.POSTER_PATH));
        String overview = getString(getColumnIndex(FavoriteTable.Cols.OVERVIEW));
        Double rating = getDouble(getColumnIndex(FavoriteTable.Cols.RATING));

        MovieItem movieItem = new MovieItem();
        movieItem.setMovieId(Integer.parseInt(movieId));
        movieItem.setTitle(title);
        movieItem.setReleaseDate(releaseDate);
        movieItem.setPosterPath(posterPath);
        movieItem.setOverview(overview);
        movieItem.setRating(rating);

        return movieItem;
    }
}

