package media.sbs.movieshack.database;

/**
 * Created by ss on 4/12/16.
 */
public class FavoriteDbSchema {

    public static final class FavoriteTable {
        public static final String NAME = "favorites";

        public static final class Cols {
            public static final String MOVIE_ID = "movieid";
            public static final String POSTER_PATH = "posterpath";
            public static final String TITLE = "title";
            public static final String OVERVIEW = "overview";
            public static final String RATING = "rating";
            public static final String RELEASE_DATE = "releasedate";
        }
    }

}
