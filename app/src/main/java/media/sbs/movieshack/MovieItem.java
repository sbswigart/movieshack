package media.sbs.movieshack;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ss on 3/30/16.
 */
public class MovieItem implements Serializable {

    private int mMovieId;
    private String mPosterPath;
    private String mTitle;
    private String mOverview;
    private Double mRating;
    private String mReleaseDate;
    private List<TrailerItem> mTrailers;
    private List<UserReviewItem> mUserReviews;
    private Boolean mIsFavorite;

    public int getMovieId() {
        return mMovieId;
    }

    public void setMovieId(int movieId) {
        mMovieId = movieId;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String posterPath) {
        mPosterPath = posterPath;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        mOverview = overview;
    }

    public Double getRating() {
        return mRating;
    }

    public void setRating(Double rating) {
        mRating = rating;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        mReleaseDate = releaseDate;
    }

    public List<TrailerItem> getTrailers() {
        return mTrailers;
    }

    public void setTrailers(List<TrailerItem> trailers) {
        mTrailers = trailers;
    }

    public List<UserReviewItem> getUserReviews() {
        return mUserReviews;
    }

    public void setUserReviews(List<UserReviewItem> userReviews) {
        mUserReviews = userReviews;
    }

    public Boolean isFavorite() {
        return mIsFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        mIsFavorite = isFavorite;
    }
}
